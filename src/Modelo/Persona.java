/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 *  Clase representa una persona con su fecha de nacimiento
 * @author madarme
 */
public class Persona implements Comparable {

    private String nombresCompletos;
    //Su fecha de nacimiento
    private short dia, mes, agno, hora, min, seg;

    public Persona() {
    }

    /**
     * Toma datos de una persona
     *
     * @param nombresCompletos un String que representa el nombre completo de la
     * persona
     * @param dia un short que representa el día de nacimiento
     * @param mes un short que representa el mes de nacimiento
     * @param agno un short que representa el año de nacimiento
     * @param hora un short que representa la hora de nacimiento
     * @param min un short que representa el minuto de nacimiento
     * @param seg un short que representa los segundos de nacimiento
     */
    public Persona(String nombresCompletos, short agno, short mes, short dia, short hora, short min, short seg) throws Exception {
        this.nombresCompletos = nombresCompletos;
        this.dia = dia;
        this.mes = mes;
        this.agno = agno;
        this.hora = hora;
        this.min = min;
        this.seg = seg;
        
    }

    public boolean comprobarDia(){
              
        short dia = this.dia;
        short mes = this.mes;
        
        boolean comprobar = ((dia <= 30 & dia >= 1) & (mes == 11 | mes == 4 | mes == 6 | mes == 9)) | ((dia <= 31 & dia >= 1) & (mes == 1 | mes == 3 | mes == 7 | mes == 8 | mes == 10 | mes == 12 | mes == 5)) | ((dia <= 28 & dia >= 1) & mes == 2);
    
        return comprobar;
    }

    
    /**
     * Retorna la cadena de texto del nombre
     *
     * @return un String con el nombre completo
     */
    public String getNombresCompletos() {
        return nombresCompletos;
    }

    /**
     * Actualiza la cadena de texto del nombre
     *
     * @param nombresCompletos nueva cadena para el nombre completo
     */
    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

    /**
     * Retorna el valor del dia
     *
     * @return un short con el dia de nacimiento
     */
    public short getDia() {
        return dia;
    }

    /**
     * Actualiza el valor del dia
     *
     * @param dia nuevo valor para el dia de nacimiento
     */
    public void setDia(short dia) {
        this.dia = dia;
    }

    /**
     * Retorna el valor del mes
     *
     * @return un short con el mes de nacimiento
     */
    public short getMes() {
        return mes;
    }

    /**
     * Actualiza el valor del mes
     *
     * @param mes nuevo valor para el mes de nacimiento
     */
    public void setMes(short mes) {
        this.mes = mes;
    }

    /**
     * Retorna el valor del año
     *
     * @return un short con el año de nacimiento
     */
    public short getAgno() {
        return agno;
    }

    /**
     * Actualiza el valor del año
     *
     * @param agno nuevo valor para el año de nacimiento
     */
    public void setAgno(short agno) {
        this.agno = agno;
    }

    /**
     * Retorna el valor de la hora
     *
     * @return un short con la hora de nacimiento
     */
    public short getHora() {
        return hora;
    }

    /**
     * Actualiza el valor de la hora
     *
     * @param hora nuevo valor para la hora de nacimiento
     */
    public void setHora(short hora) {
        this.hora = hora;
    }

    /**
     * Retorna el valor de los minutos
     *
     * @return un short con los minutos de nacimiento
     */
    public short getMin() {
        return min;
    }

    /**
     * Actualiza el valor de los minutos
     *
     * @param min nuevo valor para los minutos de nacimiento
     */
    public void setMin(short min) {
        this.min = min;
    }

    /**
     * Retorna el valor de los segundos
     *
     * @return un short con los segundos de nacimiento
     */
    public short getSeg() {
        return seg;
    }

    /**
     * Actualiza el valor de los segundos
     *
     * @param seg nuevo valor para los segundos de nacimiento
     */
    public void setSeg(short seg) {
        this.seg = seg;
    }

    /**
     * Convierte en cadena de texto el objeto Persona
     *
     * @return cadena de texto
     */
    @Override
    public String toString() {
        return "La persona " + nombresCompletos + ", nació en la siguiente fecha: " + agno + "/" + mes + "/" + dia + " y en la hora: " + hora + ":" + min + ":" + seg;
    }

    /**
     *
     * @param obj compara la fecha de nacimiento de dos personas
     * @return retorna un valor short que demuestra si nacieron en la misma
     * fecha, una persona es mayor o incluso menor
     */
    @Override
    public int compareTo(Object obj) {

        if (this == obj) {
            return 0;
        }
        if (obj == null) {
            throw new RuntimeException("No se puede comparar porque el segundo objeto es null");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("Objetos no son de la misma Clase");
        }

        final Persona other = (Persona) obj;

        short comparadorAgno = (short) (this.agno - other.agno);
        short comparadorMes = (short) (this.mes - other.mes);
        short comparadorDia = (short) (this.dia - other.dia);
        short comparadorHora = (short) (this.hora - other.hora);
        short comparadorMinuto = (short) (this.min - other.min);
        short comparadorSegundo = (short) (this.seg - other.seg);

        short resultado = (short) (comparadorAgno == 0 ? (comparadorMes == 0 ? (comparadorDia == 0 ? (comparadorHora == 0 ? (comparadorMinuto == 0 ? (comparadorSegundo) : comparadorMinuto) : comparadorHora) : comparadorDia) : comparadorMes) : comparadorAgno);

        return resultado;
    }

    /**
     *
     * @param obj comprueba si dos objetos son iguales
     * @return retorna un valor boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.dia != other.dia) {
            return false;
        }
        if (this.mes != other.mes) {
            return false;
        }
        if (this.agno != other.agno) {
            return false;
        }
        if (this.hora != other.hora) {
            return false;
        }
        if (this.min != other.min) {
            return false;
        }
        if (this.seg != other.seg) {
            return false;
        }
        return Objects.equals(this.nombresCompletos, other.nombresCompletos);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.nombresCompletos);
        hash = 53 * hash + this.dia;
        hash = 53 * hash + this.mes;
        hash = 53 * hash + this.agno;
        hash = 53 * hash + this.hora;
        hash = 53 * hash + this.min;
        hash = 53 * hash + this.seg;
        return hash;
    }

}

