/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.*;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class Test_MatrizCuadrada_Rectangular {

    public static void main(String[] args) {
        int cantFilas = leerEntero("Digite la cantidad filas?:");
        int columnas = leerEntero("Digite cuántas columnas le gustaría?:");
        //utilizo el constructor de filas:
        Matriz_Numeros myMatriz = new Matriz_Numeros(cantFilas);
        for (int i = 0; i < myMatriz.length_filas(); i++) {
            Random r = new Random();
            //Esté número puede dar 0 
            ListaNumeros myLista_columnas = new ListaNumeros(columnas);
            for (int j = 0; j < myLista_columnas.length(); j++) {
                myLista_columnas.adicionar(j, r.nextInt(100));
            }
            //Adicionando el vector que contiene las columnas a la matriz:
            myMatriz.adicionarVector(i, myLista_columnas);

        }
        
        System.out.println("Mi matriz es:\n"+myMatriz.toString());

    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerEntero(msg);
        }

    }
}
